package ru.t1.aayakovlev.tm.model;

import org.jetbrains.annotations.NotNull;

public interface HasName {

    @NotNull
    String getName();

    void setName(@NotNull final String name);

}
