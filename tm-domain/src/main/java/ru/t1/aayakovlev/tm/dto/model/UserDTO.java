package ru.t1.aayakovlev.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.enumerated.Role;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_user", schema = "public", catalog = "task_manager")
public final class UserDTO extends AbstractModelDTO {

    private static final long serialVersionUID = 0;

    @Nullable
    @Column(name = "login", columnDefinition = "VARCHAR(64)")
    private String login;

    @Nullable
    @Column(name = "password", columnDefinition = "VARCHAR(32)")
    private String passwordHash;

    @Nullable
    @Column(name = "email", columnDefinition = "VARCHAR(64)")
    private String email;

    @Nullable
    @Column(name = "first_name", columnDefinition = "VARCHAR(64)")
    private String firstName;

    @Nullable
    @Column(name = "last_name", columnDefinition = "VARCHAR(64)")
    private String lastName;

    @Nullable
    @Column(name = "middle_name", columnDefinition = "VARCHAR(64)")
    private String middleName;

    @NotNull
    @Column(name = "role", columnDefinition = "VARCHAR(64)")
    @Enumerated(EnumType.STRING)
    private Role role = Role.USUAL;

    @Column(name = "locked", columnDefinition = "BOOLEAN")
    private boolean locked = false;

    public UserDTO(@NotNull final String login, @NotNull final Role role, @NotNull String passwordHash) {
        this.login = login;
        this.role = role;
        this.passwordHash = passwordHash;
    }

}
