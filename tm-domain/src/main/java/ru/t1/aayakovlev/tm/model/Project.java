package ru.t1.aayakovlev.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.enumerated.Status;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_project", schema = "public", catalog = "task_manager")
public final class Project extends AbstractUserOwnedModel implements WBS {

    private static final long serialVersionUID = 0;

    @NotNull
    @Column(name = "created", columnDefinition = "TIMESTAMP")
    private Date created = new Date();

    @NotNull
    @Column(name = "description", columnDefinition = "VARCHAR(255)")
    private String description = "";

    @NotNull
    @Column(name = "name", columnDefinition = "VARCHAR(64)")
    private String name = "";

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status", columnDefinition = "VARCHAR(64)")
    private Status status = Status.NOT_STARTED;

    @NotNull
    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Task> tasks = new ArrayList<>();

    public Project(@NotNull final String name, @NotNull final String description) {
        this.setName(name);
        this.setDescription(description);
    }

    public Project(@NotNull final String name, @NotNull final Status status) {
        this.setName(name);
        this.setStatus(status);
    }

    @NotNull
    @Override
    public String toString() {
        return name + " : " + description;
    }

}
