package ru.t1.aayakovlev.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.enumerated.Role;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_session", schema = "public", catalog = "task_manager")
public final class Session extends AbstractUserOwnedModel {

    @NotNull
    @Column(name = "date", columnDefinition = "TIMESTAMP")
    private Date date = new Date();

    @Nullable
    @Column(name = "role", columnDefinition = "VARCHAR(64)")
    @Enumerated(EnumType.STRING)
    private Role role = null;

}
