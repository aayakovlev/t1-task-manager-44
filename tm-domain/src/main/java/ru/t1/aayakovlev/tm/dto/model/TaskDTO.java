package ru.t1.aayakovlev.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.model.WBS;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_task", schema = "public", catalog = "task_manager")
public final class TaskDTO extends AbstractExtendedModelDTO implements WBS {

    private static final long serialVersionUID = 0;

    @Nullable
    @Column(name = "project_id", columnDefinition = "VARCHAR(36)")
    private String projectId;


    @NotNull
    @Column(name = "created", columnDefinition = "TIMESTAMP")
    private Date created = new Date();

    @NotNull
    @Column(name = "description", columnDefinition = "VARCHAR(255)")
    private String description = "";

    @NotNull
    @Column(name = "name", columnDefinition = "VARCHAR(64)")
    private String name = "";

    @NotNull
    @Column(name = "status", columnDefinition = "VARCHAR(64)")
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    public TaskDTO(@NotNull final String name, @NotNull final String description) {
        this.setName(name);
        this.setDescription(description);
    }

    public TaskDTO(@NotNull final String name, @NotNull final Status status) {
        this.setName(name);
        this.setStatus(status);
    }

    @NotNull
    @Override
    public String toString() {
        return name + " : " + description;
    }

}
