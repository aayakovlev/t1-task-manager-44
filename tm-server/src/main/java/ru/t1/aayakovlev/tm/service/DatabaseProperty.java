package ru.t1.aayakovlev.tm.service;

import org.jetbrains.annotations.NotNull;

public interface DatabaseProperty {

    @NotNull
    String getDatabaseDriver();

    @NotNull
    String getDatabasePassword();

    @NotNull
    String getDatabaseUser();

    @NotNull
    String getDatabaseURL();

    @NotNull
    String getDatabaseDialect();

    @NotNull
    String getDatabaseHBM2DLL();

    @NotNull
    String getDataBaseShowSql();

    @NotNull
    String getDataBaseSchema();

}
