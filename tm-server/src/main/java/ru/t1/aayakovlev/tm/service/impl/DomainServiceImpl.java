package ru.t1.aayakovlev.tm.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.Cleanup;
import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.dto.Domain;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.service.AuthService;
import ru.t1.aayakovlev.tm.service.dto.ProjectDTOService;
import ru.t1.aayakovlev.tm.service.dto.TaskDTOService;
import ru.t1.aayakovlev.tm.service.dto.UserDTOService;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public final class DomainServiceImpl implements ru.t1.aayakovlev.tm.service.DomainService {

    @NotNull
    public static final String FILE_BACKUP = "./backup.json";

    @NotNull
    public static final String FILE_BASE64 = "./data.base64";

    @NotNull
    public static final String FILE_BIN = "./data.bin";

    @NotNull
    public static final String FILE_XML = "./data.xml";

    @NotNull
    public static final String FILE_JSON = "./data.json";

    @NotNull
    public static final String FILE_YAML = "./data.yaml";

    @NotNull
    public static final String CONTEXT_FACTORY = "javax.xml.bind.context.factory";

    @NotNull
    public static final String CONTEXT_FACTORY_JAXB = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    @NotNull
    public static final String MEDIA_TYPE = "eclipselink.media-type";

    @NotNull
    public static final String APPLICATION_TYPE_JSON = "application/json";

    @Getter
    @NotNull
    private final AuthService authService;

    @Getter
    @NotNull
    private final ProjectDTOService projectService;

    @Getter
    @NotNull
    private final TaskDTOService taskService;

    @Getter
    @NotNull
    private final UserDTOService userService;

    public DomainServiceImpl(
            @NotNull final AuthService authService,
            @NotNull final ProjectDTOService projectService,
            @NotNull final TaskDTOService taskService,
            @NotNull final UserDTOService userService
    ) {
        this.authService = authService;
        this.projectService = projectService;
        this.taskService = taskService;
        this.userService = userService;
    }

    @NotNull
    @Override
    public Domain getDomain() throws AbstractException {
        @NotNull final Domain domain = new Domain();
        domain.setUsers(getUserService().findAll());
        domain.setProjects(getProjectService().findAll());
        domain.setTasks(getTaskService().findAll());
        return domain;
    }

    @Override
    public void setDomain(@Nullable final Domain domain) throws AbstractException {
        if (domain == null) return;
        getUserService().set(domain.getUsers());
        getProjectService().set(domain.getProjects());
        getTaskService().set(domain.getTasks());
    }

    @Override
    @SneakyThrows
    public void backupLoad() {
        @NotNull final byte[] bytes = Files.readAllBytes(Paths.get(FILE_BACKUP));
        @NotNull final String json = new String(bytes);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Domain domain = objectMapper.readValue(json, Domain.class);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void backupSave() {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_BACKUP);
        @NotNull final Path path = file.toPath();

        Files.deleteIfExists(path);
        Files.createFile(path);

        @Cleanup @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
    }

    @Override
    @SneakyThrows
    public void base64Load() {
        @NotNull final byte[] base64Byte = Files.readAllBytes(Paths.get(FILE_BASE64));
        @Nullable final String base64Data = new String(base64Byte);
        @Nullable final byte[] bytes = new BASE64Decoder().decodeBuffer(base64Data);
        @Cleanup @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        @Cleanup @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void base64Save() {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_BASE64);
        @NotNull final Path path = file.toPath();

        Files.deleteIfExists(path);
        Files.createFile(path);

        @Cleanup @NotNull final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        @Cleanup @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(domain);

        @NotNull final byte[] bytes = byteArrayOutputStream.toByteArray();
        @NotNull final String base64 = new BASE64Encoder().encode(bytes);

        @Cleanup @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(base64.getBytes());
        fileOutputStream.flush();
    }

    @Override
    @SneakyThrows
    public void binaryLoad() {
        @Cleanup @NotNull final FileInputStream fileInputStream = new FileInputStream(FILE_BIN);
        @Cleanup @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void binarySave() {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_BIN);
        @NotNull final Path path = file.toPath();

        Files.deleteIfExists(path);
        Files.createFile(path);

        @Cleanup @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @Cleanup @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(domain);
    }

    @Override
    @SneakyThrows
    public void jsonLoadFXml() {
        @NotNull final byte[] bytes = Files.readAllBytes(Paths.get(FILE_JSON));
        @NotNull final String json = new String(bytes);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Domain domain = objectMapper.readValue(json, Domain.class);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void jsonLoadJaxB() {
        System.setProperty(CONTEXT_FACTORY, CONTEXT_FACTORY_JAXB);
        @NotNull JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        unmarshaller.setProperty(MEDIA_TYPE, APPLICATION_TYPE_JSON);
        @NotNull final File file = new File(FILE_JSON);
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void jsonSaveFXml() {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_JSON);
        @NotNull final Path path = file.toPath();

        Files.deleteIfExists(path);
        Files.createFile(path);

        @Cleanup @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
    }

    @Override
    @SneakyThrows
    public void jsonSaveJaxB() {
        System.setProperty(CONTEXT_FACTORY, CONTEXT_FACTORY_JAXB);
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_JSON);
        @NotNull final Path path = file.toPath();

        Files.deleteIfExists(path);
        Files.createFile(path);

        @NotNull JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.setProperty(MEDIA_TYPE, APPLICATION_TYPE_JSON);
        @Cleanup @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        jaxbMarshaller.marshal(domain, fileOutputStream);
        fileOutputStream.flush();
    }

    @Override
    @SneakyThrows
    public void xmlLoadFXml() {
        @NotNull final byte[] bytes = Files.readAllBytes(Paths.get(FILE_XML));
        @NotNull final String xml = new String(bytes);
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final Domain domain = objectMapper.readValue(xml, Domain.class);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void xmlLoadJaxB() {
        @NotNull JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @NotNull final File file = new File(FILE_XML);
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void xmlSaveFXml() {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_XML);
        @NotNull final Path path = file.toPath();

        Files.deleteIfExists(path);
        Files.createFile(path);

        @Cleanup @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        fileOutputStream.write(xml.getBytes());
        fileOutputStream.flush();
    }

    @Override
    @SneakyThrows
    public void xmlSaveJaxB() {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_XML);
        @NotNull final Path path = file.toPath();

        Files.deleteIfExists(path);
        Files.createFile(path);

        @NotNull JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        @Cleanup @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        jaxbMarshaller.marshal(domain, fileOutputStream);
        fileOutputStream.flush();
    }

    @Override
    @SneakyThrows
    public void yamlLoad() {
        @NotNull final byte[] bytes = Files.readAllBytes(Paths.get(FILE_YAML));
        @NotNull final String xml = new String(bytes);
        @NotNull final ObjectMapper objectMapper = new YAMLMapper();
        @NotNull final Domain domain = objectMapper.readValue(xml, Domain.class);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void yamlSave() {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_YAML);
        @NotNull final Path path = file.toPath();

        Files.deleteIfExists(path);
        Files.createFile(path);

        @Cleanup @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull final ObjectMapper objectMapper = new YAMLMapper();
        @NotNull final String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        fileOutputStream.write(xml.getBytes());
        fileOutputStream.flush();
    }

}
