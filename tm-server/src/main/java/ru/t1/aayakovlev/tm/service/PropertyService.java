package ru.t1.aayakovlev.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.endpoint.ConnectionProvider;

public interface PropertyService extends SaltProvider, ConnectionProvider, DatabaseProperty, BackupService {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getApplicationConfig();

    @NotNull
    String getAuthorName();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getGitBranch();

    @NotNull
    String getGitCommitId();

    @NotNull
    String getGitCommitterName();

    @NotNull
    String getGitCommitterEmail();

    @NotNull
    String getGitCommitMessage();

    @NotNull
    String getGitCommitTime();

    @NotNull
    String getSessionKey();

    @NotNull
    Integer getSessionTimeout();

}
