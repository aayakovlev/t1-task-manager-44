package ru.t1.aayakovlev.tm.service.dto.impl;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.model.SessionDTO;
import ru.t1.aayakovlev.tm.repository.dto.SessionDTORepository;
import ru.t1.aayakovlev.tm.repository.dto.impl.SessionDTORepositoryImpl;
import ru.t1.aayakovlev.tm.service.ConnectionService;
import ru.t1.aayakovlev.tm.service.dto.SessionDTOService;

import javax.persistence.EntityManager;

public final class SessionDTOServiceImpl extends AbstractExtendedDTOService<SessionDTO, SessionDTORepository> implements SessionDTOService {

    public SessionDTOServiceImpl(@NotNull final ConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected SessionDTORepository getRepository(@NotNull final EntityManager entityManager) {
        return new SessionDTORepositoryImpl(entityManager);
    }

}
