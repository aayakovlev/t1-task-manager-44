package ru.t1.aayakovlev.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.dto.model.AbstractModelDTO;

import java.util.Comparator;
import java.util.List;

public interface BaseDTORepository<E extends AbstractModelDTO> {

    E save(@NotNull final E entity);

    void clear();

    @NotNull
    List<E> findAll();

    @NotNull
    List<E> findAll(@NotNull final Comparator<E> comparator);

    @Nullable
    E findById(@NotNull final String id);

    int count();

    void removeById(@NotNull final String id);

    E update(@NotNull final E entity);

}
