package ru.t1.aayakovlev.tm.service;

import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;

public interface ConnectionService {

    @NotNull
    EntityManager getEntityManager();

}
