package ru.t1.aayakovlev.tm.repository.dto.impl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.dto.model.AbstractExtendedModelDTO;
import ru.t1.aayakovlev.tm.repository.dto.ExtendedDTORepository;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractExtendedDTORepository<E extends AbstractExtendedModelDTO>
        extends AbstractBaseDTORepository<E> implements ExtendedDTORepository<E> {

    public AbstractExtendedDTORepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public int count(@NotNull final String userId) {
        @NotNull final String query = "select count(*) from " + getClazz().getSimpleName() + " e " +
                "where e.userId = :userId";
        return entityManager.createQuery(query, Long.class)
                .setParameter("userId", userId)
                .getSingleResult()
                .intValue();
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final String query = "delete from " + getClazz().getSimpleName() + " e " +
                "where e.userId = :userId";
        entityManager.createQuery(query)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId) {
        @NotNull final String query = "from " + getClazz().getSimpleName() + " e " +
                "where e.userId = :userId";
        return entityManager.createQuery(query, getClazz())
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId, @NotNull final Comparator<E> comparator) {
        @NotNull final String query = "from " + getClazz().getSimpleName() + " e " +
                "where e.userId = :userId order by " + getSortColumnName(comparator);
        return entityManager.createQuery(query, getClazz())
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    @Nullable
    public E findById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String query = "from " + getClazz().getSimpleName() + " e " +
                "where e.userId = :userId and e.id = :id";
        return entityManager.createQuery(query, getClazz())
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1).getSingleResult();
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String query = "delete from " + getClazz().getSimpleName() + " e " +
                "where e.id = :id and e.userId = :userId";
        entityManager.createQuery(query).executeUpdate();
    }

}
