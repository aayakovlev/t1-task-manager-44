package ru.t1.aayakovlev.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.service.DomainService;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static ru.t1.aayakovlev.tm.service.impl.DomainServiceImpl.FILE_BACKUP;

public final class Backup {

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @NotNull
    private DomainService getDomainService() {
        return bootstrap.getDomainService();
    }

    public void init() {
        load();
        es.scheduleWithFixedDelay(this::save, 0, 10, TimeUnit.SECONDS);
    }

    @SneakyThrows
    public void load() {
        if (!Files.exists(Paths.get(FILE_BACKUP))) return;
        getDomainService().backupLoad();
    }

    @SneakyThrows
    public void save() {
        getDomainService().backupSave();
    }

    public void stop() {
        es.shutdown();
    }

}
