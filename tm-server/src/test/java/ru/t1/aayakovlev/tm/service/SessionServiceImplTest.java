package ru.t1.aayakovlev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.aayakovlev.tm.dto.model.SessionDTO;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.marker.UnitCategory;
import ru.t1.aayakovlev.tm.service.dto.SessionDTOService;
import ru.t1.aayakovlev.tm.service.dto.impl.SessionDTOServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.ConnectionServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.PropertyServiceImpl;

import static ru.t1.aayakovlev.tm.constant.SessionTestConstant.*;

@Category(UnitCategory.class)
public final class SessionServiceImplTest {

    @NotNull
    private static final PropertyService propertyService = new PropertyServiceImpl();

    @NotNull
    private static final ConnectionService connectionService = new ConnectionServiceImpl(propertyService);

    @NotNull
    private static final SessionDTOService service = new SessionDTOServiceImpl(connectionService);

    @BeforeClass
    public static void init() throws AbstractException {
        service.save(SESSION_USER_ONE);
        service.save(SESSION_USER_TWO);
    }

    @AfterClass
    public static void after() throws AbstractException {
        service.clear();
    }

    @Test
    public void When_FindByIdExistsSESSION_Expect_ReturnSession() throws AbstractException {
        @Nullable final SessionDTO session = service.findById(SESSION_USER_ONE.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(SESSION_USER_ONE.getUserId(), session.getUserId());
    }

    @Test
    public void When_FindByIdExistsSESSION_Expect_ThrowsEntityNotFoundException() {
        Assert.assertThrows(EntityNotFoundException.class,
                () -> service.findById(SESSION_ID_NOT_EXISTED)
        );

    }

    @Test
    public void When_SaveNotNullSESSION_Expect_ReturnSession() throws AbstractException {
        @NotNull final SessionDTO savedSession = service.save(SESSION_ADMIN_ONE);
        Assert.assertNotNull(savedSession);
        Assert.assertEquals(SESSION_ADMIN_ONE, savedSession);
        @Nullable final SessionDTO session = service.findById(SESSION_ADMIN_ONE.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(SESSION_ADMIN_ONE, session);
    }

    @Test
    public void When_CountCommonUserSessions_Expect_ReturnTwo() throws AbstractException {
        final int count = service.count();
        Assert.assertEquals(2, count);
    }

    @Test
    public void When_ExistsByIdExistedSession_Expected_ReturnTrue() throws AbstractException {
        final boolean exists = service.existsById(SESSION_USER_ONE.getId());
        Assert.assertTrue(exists);
    }

    @Test
    public void When_ExistsByIdNotExistedSession_Expected_ReturnFalse() throws AbstractException {
        final boolean exists = service.existsById(SESSION_ID_NOT_EXISTED);
        Assert.assertFalse(exists);
    }

    @Test
    public void When_ExistsByIdWithUserIdExistedSession_Expected_ReturnTrue() throws AbstractException {
        final boolean exists = service.existsById(SESSION_USER_ONE.getId());
        Assert.assertTrue(exists);
    }

    @Test
    public void When_ExistsByIdWithUserIdNotExistedSession_Expected_ReturnFalse() throws AbstractException {
        final boolean exists = service.existsById(SESSION_ID_NOT_EXISTED);
        Assert.assertFalse(exists);
    }

    @Test
    public void When_RemoveExistedSession_Expect_ReturnSession() throws AbstractException {
        Assert.assertNotNull(service.save(SESSION_ADMIN_TWO));
        service.removeById(SESSION_ADMIN_TWO.getId());
    }

    @Test
    public void When_RemoveNotSession_Expect_ThrowsEntityNotFoundException() throws AbstractException {
        service.removeById(SESSION_NOT_EXISTED.getId());
    }

    @Test
    public void When_RemoveAll_Expect_ZeroCountSessions() throws AbstractException {
        service.save(SESSION_ADMIN_ONE);
        service.save(SESSION_ADMIN_TWO);
        service.clear();
        Assert.assertEquals(0, service.count());
    }

    @Test
    public void When_RemoveByIdExistedSession_Expect_Session() throws AbstractException {
        Assert.assertNotNull(service.save(SESSION_ADMIN_TWO));
        service.removeById(SESSION_ADMIN_TWO.getId());
    }

}
