package ru.t1.aayakovlev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;
import ru.t1.aayakovlev.tm.enumerated.Sort;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.exception.entity.TaskNotFoundException;
import ru.t1.aayakovlev.tm.marker.UnitCategory;
import ru.t1.aayakovlev.tm.service.dto.TaskDTOService;
import ru.t1.aayakovlev.tm.service.dto.impl.TaskDTOServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.ConnectionServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.PropertyServiceImpl;

import java.util.List;

import static ru.t1.aayakovlev.tm.constant.ProjectTestConstant.PROJECT_USER_ONE;
import static ru.t1.aayakovlev.tm.constant.TaskTestConstant.*;
import static ru.t1.aayakovlev.tm.constant.UserTestConstant.*;

@Category(UnitCategory.class)
public final class TaskServiceImplTest {

    @NotNull
    private static final PropertyService propertyService = new PropertyServiceImpl();

    @NotNull
    private static final ConnectionService connectionService = new ConnectionServiceImpl(propertyService);

    @NotNull
    private static final TaskDTOService service = new TaskDTOServiceImpl(connectionService);

    @BeforeClass
    public static void init() throws AbstractException {
        service.save(TASK_USER_ONE);
        service.save(TASK_USER_TWO);
    }

    @AfterClass
    public static void after() throws AbstractException {
        service.clear();
    }

    @Test
    public void When_FindByIdExistsProject_Expect_ReturnProject() throws AbstractException {
        @Nullable final TaskDTO task = service.findById(COMMON_USER_ONE.getId(), TASK_USER_ONE.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(TASK_USER_ONE.getDescription(), task.getDescription());
        Assert.assertEquals(TASK_USER_ONE.getName(), task.getName());
        Assert.assertEquals(TASK_USER_ONE.getStatus(), task.getStatus());
        Assert.assertEquals(TASK_USER_ONE.getUserId(), task.getUserId());
        Assert.assertEquals(TASK_USER_ONE.getCreated(), task.getCreated());
    }

    @Test
    public void When_FindByIdExistsProject_Expect_ThrowsEntityNotFoundException() {
        Assert.assertThrows(TaskNotFoundException.class,
                () -> service.findById(USER_ID_NOT_EXISTED, TASK_ID_NOT_EXISTED)
        );

    }

    @Test
    public void When_SaveNotNullProject_Expect_ReturnProject() throws AbstractException {
        @NotNull final TaskDTO savedTask = service.save(TASK_ADMIN_ONE);
        Assert.assertNotNull(savedTask);
        Assert.assertEquals(TASK_ADMIN_ONE, savedTask);
        @Nullable final TaskDTO task = service.findById(ADMIN_USER_ONE.getId(), TASK_ADMIN_ONE.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(TASK_ADMIN_ONE, task);
    }

    @Test
    public void When_CountCommonUserProjects_Expect_ReturnTwo() throws AbstractException {
        final int count = service.count(COMMON_USER_ONE.getId());
        Assert.assertEquals(2, count);
    }

    @Test
    public void When_FindAllUserIdSorted_Expect_ReturnSortedProjectList() throws AbstractException {
        @NotNull final List<TaskDTO> tasks = service.findAll(COMMON_USER_ONE.getId(), Sort.BY_NAME);
        Assert.assertArrayEquals(tasks.toArray(), USER_TASK_SORTED_LIST.toArray());
    }

    @Test
    public void When_FindAllByProjectId_Expect_ReturnTasks() throws AbstractException {
        Assert.assertEquals(1, service.findAllByProjectId(COMMON_USER_ONE.getId(), PROJECT_USER_ONE.getId()).size());
    }

    @Test
    public void When_RemoveExistedProject_Expect_ReturnProject() throws AbstractException {
        Assert.assertNotNull(service.save(TASK_ADMIN_TWO));
        service.removeById(ADMIN_USER_ONE.getId(), TASK_ADMIN_TWO.getId());
    }

    @Test
    public void When_RemoveNotProject_Expect_ThrowsEntityNotFoundException() throws AbstractException {
        service.removeById(ADMIN_USER_ONE.getId(), TASK_NOT_EXISTED.getId());
    }

    @Test
    public void When_RemoveAll_Expect_ZeroCountProjects() throws AbstractException {
        service.save(TASK_ADMIN_ONE);
        service.save(TASK_ADMIN_TWO);
        service.clear(ADMIN_USER_ONE.getId());
        Assert.assertEquals(0, service.count(ADMIN_USER_ONE.getId()));
    }

    @Test
    public void When_RemoveByIdExistedProject_Expect_Project() throws AbstractException {
        Assert.assertNotNull(service.save(TASK_ADMIN_TWO));
        service.removeById(ADMIN_USER_ONE.getId(), TASK_ADMIN_TWO.getId());
    }

    @Test
    public void When_RemoveByIdNotExistedProject_Expect_ThrowsEntityNotFoundException() {
        Assert.assertThrows(
                EntityNotFoundException.class,
                () -> service.removeById(ADMIN_USER_ONE.getId(), TASK_NOT_EXISTED.getId())
        );
    }

    @Test
    public void When_CreateNameProject_Expect_ExistedProject() throws AbstractException {
        @Nullable final TaskDTO task = service.create(ADMIN_USER_ONE.getId(), NAME);
        Assert.assertNotNull(task);
        Assert.assertEquals(NAME, task.getName());
    }

    @Test
    public void When_CreateNameDescriptionProject_Expect_ExistedProject() throws AbstractException {
        @Nullable final TaskDTO task = service.create(ADMIN_USER_ONE.getId(), NAME, DESCRIPTION);
        Assert.assertNotNull(task);
        Assert.assertEquals(NAME, task.getName());
        Assert.assertEquals(DESCRIPTION, task.getDescription());
    }

    @Test
    public void When_UpdateByIdProject_Expect_UpdatedProject() throws AbstractException {
        service.save(TASK_ADMIN_TWO);
        @Nullable TaskDTO task = service.update(
                TASK_ADMIN_TWO.getUserId(), TASK_ADMIN_TWO.getId(), NAME, DESCRIPTION
        );
        Assert.assertEquals(NAME, task.getName());
        Assert.assertEquals(DESCRIPTION, task.getDescription());
    }

}
