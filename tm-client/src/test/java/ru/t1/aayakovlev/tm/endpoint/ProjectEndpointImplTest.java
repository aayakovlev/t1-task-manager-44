package ru.t1.aayakovlev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;
import ru.t1.aayakovlev.tm.dto.request.*;
import ru.t1.aayakovlev.tm.enumerated.Sort;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.marker.IntegrationCategory;
import ru.t1.aayakovlev.tm.service.PropertyService;
import ru.t1.aayakovlev.tm.service.impl.PropertyServiceImpl;

import static ru.t1.aayakovlev.tm.constant.ProjectTestConstant.*;
import static ru.t1.aayakovlev.tm.constant.UserEndpointTestConstant.*;

@Category(IntegrationCategory.class)
public final class ProjectEndpointImplTest {

    @NotNull
    private final PropertyService propertyService = new PropertyServiceImpl();

    @NotNull
    private final AuthEndpoint authEndpoint = AuthEndpoint.newInstance(propertyService);

    @NotNull
    private final UserEndpoint userEndpoint = UserEndpoint.newInstance(propertyService);

    @NotNull
    private final ProjectEndpoint projectEndpoint = ProjectEndpoint.newInstance(propertyService);

    @Nullable
    private String adminToken;

    @Nullable
    private String ID;

    @Nullable
    private String DELETE_ID;

    @Before
    public void init() throws Exception {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setLogin(ADMIN_LOGIN);
        loginRequest.setPassword(ADMIN_PASSWORD);
        adminToken = authEndpoint.login(loginRequest).getToken();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(adminToken);
        request.setLogin(TEST_LOGIN);
        request.setPassword(TEST_PASSWORD);
        request.setEmail(TEST_EMAIL);
        userEndpoint.registry(request);
        ID = create().getId();
        create();
        DELETE_ID = create().getId();
    }

    @After
    public void after() throws AbstractException {
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(adminToken);
        request.setLogin(TEST_LOGIN);
        userEndpoint.remove(request);
    }

    @NotNull
    private String getToken() throws AbstractException {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setLogin(TEST_LOGIN);
        loginRequest.setPassword(TEST_PASSWORD);
        return authEndpoint.login(loginRequest).getToken();
    }

    private ProjectDTO create() throws AbstractException {
        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(getToken());
        projectCreateRequest.setName("Test Project name");
        projectCreateRequest.setDescription("Test Project name");
        return projectEndpoint.createProject(projectCreateRequest).getProject();
    }

    @Test
    public void When_ProjectChangeStatusById_Expect_ProjectChangeStatusByIdResponse() throws AbstractException {
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(getToken());
        request.setId(ID);
        request.setStatus(STATUS);

        Assert.assertNotNull(projectEndpoint.changeProjectStatusById(request));
    }

    @Test
    public void When_ProjectCreate_Expect_ProjectCreateResponse() throws AbstractException {
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(getToken());
        request.setName(NAME);
        request.setDescription(DESCRIPTION);

        Assert.assertNotNull(projectEndpoint.createProject(request));
    }

    @Test
    public void When_ProjectClear_Expect_ProjectClearResponse() throws AbstractException {
        @NotNull final ProjectClearRequest request = new ProjectClearRequest(adminToken);

        Assert.assertNotNull(projectEndpoint.clearProjects(request));
    }

    @Test
    public void When_ProjectRemoveById_Expect_ProjectRemoveByIdResponse() throws AbstractException {
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(getToken());
        request.setId(DELETE_ID);

        Assert.assertNotNull(projectEndpoint.removeProjectById(request));
    }

    @Test
    public void When_ProjectShowAll_Expect_ProjectShowAllResponse() throws AbstractException {
        @NotNull final ProjectShowAllRequest request = new ProjectShowAllRequest(getToken());
        request.setSort(Sort.BY_NAME);

        Assert.assertNotNull(projectEndpoint.showAllProjects(request));
    }

    @Test
    public void When_ProjectShowById_Expect_ProjectShowByIdResponse() throws AbstractException {
        @NotNull final ProjectShowByIdRequest request = new ProjectShowByIdRequest(getToken());
        request.setId(ID);

        Assert.assertNotNull(projectEndpoint.showProjectById(request));
    }

    @Test
    public void When_ProjectUpdateById_Expect_ProjectUpdateByIdResponse() throws AbstractException {
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(getToken());
        request.setId(ID);
        request.setName(NAME);
        request.setDescription(DESCRIPTION);

        Assert.assertNotNull(projectEndpoint.updateProjectById(request));
    }

}
