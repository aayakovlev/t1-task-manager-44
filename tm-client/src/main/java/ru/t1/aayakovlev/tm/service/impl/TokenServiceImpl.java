package ru.t1.aayakovlev.tm.service.impl;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.service.TokenService;

@Getter
@Setter
public final class TokenServiceImpl implements TokenService {

    @Nullable
    private String token;

}
