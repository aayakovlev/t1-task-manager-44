package ru.t1.aayakovlev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.command.AbstractCommand;
import ru.t1.aayakovlev.tm.endpoint.SystemEndpoint;
import ru.t1.aayakovlev.tm.service.CommandService;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    protected CommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    @NotNull
    protected SystemEndpoint getSystemEndpoint() {
        return serviceLocator.getSystemEndpoint();
    }

}
