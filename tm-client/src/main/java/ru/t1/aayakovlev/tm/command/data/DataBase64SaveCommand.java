package ru.t1.aayakovlev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.request.DataBase64SaveRequest;
import ru.t1.aayakovlev.tm.exception.AbstractException;

public final class DataBase64SaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-base64-save";

    @NotNull
    private static final String DESCRIPTION = "Save data to base64 file.";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @SneakyThrows
    public void execute() throws AbstractException {
        System.out.println("[DATA SAVE BASE64]");
        getDomainEndpoint().base64Save(new DataBase64SaveRequest(getToken()));
    }

}
